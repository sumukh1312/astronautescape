﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    [SerializeField] float xValue = 0f;
    [SerializeField] float yValue = 0f;
    [SerializeField] float zValue = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity = new Vector3(xValue,yValue,zValue);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
