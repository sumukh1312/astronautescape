﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    [SerializeField] float xAxis = 0f;
    [SerializeField] float yAxis = 0f;
    [SerializeField] float zAxis = 0f;

    void Update()
    {
        transform.Rotate(xAxis * Time.deltaTime,yAxis * Time.deltaTime,zAxis * Time.deltaTime);
    }
}
