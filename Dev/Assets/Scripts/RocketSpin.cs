﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketSpin : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        this.transform.RotateAround(this.transform.parent.position,this.transform.parent.up,500f * Time.deltaTime); 
    }
}
